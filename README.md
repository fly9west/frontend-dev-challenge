## Front-end Programming Task
In order to be considered for the front-end position, you must complete the following steps. 

*Note: This task should take no longer than 1-2 hours at the most.*
### Prerequisites
- Please note that this will require some basic [JavaScript](http://www.codecademy.com/tracks/javascript), [AngularJS](https://angularjs.org/), HTML5 and CSS3 knowledge. 
## Task
1. Fork this repository (if you don't know how to do that, Google is your friend)
2. Create a *source* folder to contain your code. 
3. In the *source* directory, please add HTML/CSS3/JS code that accomplishes the following:
	- Connect to the [Github API](http://developer.github.com/)
	- Find the [angular/angular.js](https://github.com/angular/angular.js) repository
	- Find the most recent commits (choose at least 25 or more of the commits)
	- Creates a view that groups the recent commits by author in a list layout, one commit per line. This is an [example screenshot](example-list-cell.png) of what each cell should look like. If the commit hash ends in a number, color the background of that row to light blue (#E6F1F6). This entire view should be in the center of the page.
	- Add a Refresh button to refresh the page. This refresh button should be written using CSS properties alone. [Click here](example-button.png) to see an example of the button. The button should be in the center of the page.
4. Commit and Push your code to your fork
5. Send us a pull request, we will review your code and get back to you